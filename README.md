# Solr Indexer

This microservice send updates and deletes to the SOLR server. It can be run either as an updater (using indexmode=update) or as a deleter (using indexmode=delete). A single Solr Indexer cannot do both, you need to run separate instances for updaters and deleters. The deleter mode only uses the id to delete them from solr

# Environment variables

Here are the environment variables :

* `NODE_URL` Url of the solr server
* `SOLR_COLLECTION` Name of the solr collection where the records are located
* `CONNECTION_TIMEOUT` Solr connection timeout
* `SOCKET_TIMEOUT` Solr socket timeout
* `KAFKA_BOOTSTRAP_SERVERS` Kafka cluster address
* `CLIENT_ID` Client id of the Kafka Stream application
* `TOPIC_IN` the kafka topic we read from
* `SOLR_COMMIT_BUFFER_LENGTH` if the buffer of the solr indexer have more documents than this value, then they are sent to SOLR
* `TIME_DURATION_FOR_NEXT_SOLR_COMMIT` this is a duration in minutes (should be an integer). After this duration, the documents in the buffer are sent to SOLR even if the buffer size is smaller than `SOLR_COMMIT_BUFFER_LENGTH`
* `INDEXMODE` could be either `update` if we want to update the records in solr or `delete` if we want to delete the records
* `MODE` the way to communicate to the solr cluster. Could be `node` (which is what we use) or `cloud`. `cloud` is using zookeeper, `node` is using solr server DNS. We should use `cloud`, but currently we use `node`

The following environment variable are only used if we are in solr cloud mode. But they always need to be set.

* `ZOOKEEPER_NAMESPACE` namespace of zookeeper to use (something like /solr). NONE in case no zookeeper namespace is used
* `ZKHOSTS` url to the zookeeper hosts (separated by ,)


# Unit Tests

```
sbt run test
```

# Integration Tests

There are two external systems needed : a SOLR cluster and a kafka cluster. You can do some integration testing manually. In `docker-compose.yml`, you can find a setup for a Kafka Cluster via docker-compose.

How to test the whole system.

Launch the kafka cluster and create the topics :

```
make up
```

Produce some test data :

```
./scripts/produce-test-data.sh
```

If needed : create a tunnel to solr test cluster.

Set the environment variables (in Intellij or via the following script) :
```
source ./scripts/setEnvs.sh
```
Run the program by clicking the green arrow in Main.scala in IntelliJ

It should send the updates from /data/records.txt to solr.

Inspiration : https://github.com/mitch-seymour/mastering-kafka-streams-and-ksqldb/tree/master/chapter-04
