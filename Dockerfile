FROM eclipse-temurin:21-jre-noble
ADD target/app.jar /app/app.jar
CMD java -jar /app/app.jar
