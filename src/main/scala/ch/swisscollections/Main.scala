/*
 * SOLR Indexer
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import java.time.Duration
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.logging.log4j.scala.Logging
import ch.memobase.settings.SettingsLoader
import ch.swisscollections.Utils.IndexMode
import org.joda.time.DateTime

import scala.jdk.CollectionConverters._


object Main extends App with Logging {
  val settings = new SettingsLoader(List(
    "mode",
    "zkhosts",
    "collection",
    "connectionTimeout",
    "socketTimeOut",
    "nodeURL",
    "zkChroot",
    "timeDurationForNextSolrCommit",
    "solrCommitBufferLength",
    "indexmode"
  ).asJava,
    "app.yml",
    false,
    false,
    true,
    false)

  logger.info("starting service with the following config settings")
  settings.getAppSettings.forEach((key,value) => logger.info(s"key: $key - value: $value"))
  settings.getKafkaConsumerSettings.forEach((key,value) => logger.info(s"key: $key - value: $value"))

  val indexmode = IndexMode.withName(settings.getAppSettings.getProperty("indexmode").toUpperCase)

  val solrWrapper = SolrJClientWrapper(
    settings.getAppSettings.getProperty("mode"),
    settings.getAppSettings.getProperty("zkhosts"),
    settings.getAppSettings.getProperty("collection"),
    settings.getAppSettings.getProperty("connectionTimeout"),
    settings.getAppSettings.getProperty("socketTimeOut"),
    if (settings.getAppSettings.getProperty("zkChroot") == "NONE") Option.empty else
      Option(settings.getAppSettings.getProperty("zkChroot")) ,
    settings.getAppSettings.getProperty("nodeURL"),
    settings.getAppSettings.getProperty("solrCommitBufferLength"),
    settings.getAppSettings.getProperty("timeDurationForNextSolrCommit"),
    indexmode
  )

  val pollTime = 3000 //in ms, the frequency at which we pull records from kafka
  var props = settings.getKafkaConsumerSettings
  val consumer = new KafkaConsumer[String, String](props)
  try {
    logger.debug(s"Subscribing to topic ${settings.getInputTopic}")
    consumer.subscribe(List(settings.getInputTopic).asJava)

    while (true) {
      val solrDocs = consumer.poll(Duration.ofMillis(pollTime)).asScala
      val docsWithKey: Iterable[(String, String)] = for (doc <- solrDocs)
        yield (doc.key(),doc.value())
      if (docsWithKey.nonEmpty) {
        indexmode match {
          case IndexMode.UPDATE =>
            solrWrapper.processSolrDoc(docsWithKey) match {
              case Committed =>
                consumer.commitSync()
              case _ =>
            }
          case IndexMode.DELETE =>
            solrWrapper.processSolrDeletedDoc(docsWithKey) match {
              case Committed =>
                consumer.commitSync()
              case _ =>
            }
          case _ =>
            logger.info("Invalid solr index mode")
        }
      } else {
        solrWrapper.checkLatestCommit() match {
          case Committed =>
            consumer.commitSync()
          case _ =>
        }
      }

    }

  } catch {
    case e: Exception =>
      logger.error(e)
      sys.exit(1)
  } finally {
    logger.info("Shutting down application")
    consumer.close()
  }


}
