/*
 * SOLR Indexer
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections

import ch.swisscollections.SolrJClientWrapper.getIdFromKafkaKey
import ch.swisscollections.Utils.IndexMode
import org.apache.logging.log4j.scala.Logging
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.{CloudSolrClient, HttpSolrClient}
import org.apache.solr.common.SolrInputDocument
import org.joda.time.{DateTime, Minutes}

import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters._
import scala.collection.mutable.ArrayBuffer
import scala.xml.{NodeSeq, XML}
import scala.util.{Failure, Success, Try}


class SolrJClientWrapper private (client:SolrClient,
                                  collection:String,
                                 commitBufferLength:Int,
                                  timeDurationForNextSolrCommit: Int, indexmode: Utils.IndexMode.Value) extends Logging {

  private var bufferUpdate = new ArrayBuffer[SolrInputDocument]() //in update mode, the place where the solr document are stored
  private var bufferDelete = new ArrayBuffer[String]() //in delete mode, the place where document id's are stored

  //the timestamp of the last time at which documents were sent to solr
  //at the beginning : the timestamp when the SolrJClientWrapper is instantiated
  private var timeLastCommit:DateTime = DateTime.now()


  def checkLatestCommit(): SolrClientResult = {
    val timeSinceLastCommit = Minutes.minutesBetween(timeLastCommit, DateTime.now()).getMinutes
    if(timeSinceLastCommit >= timeDurationForNextSolrCommit & (bufferDelete.nonEmpty | bufferUpdate.nonEmpty)) {
      logger.info("Time exceeded, commit to solr")
      indexmode match {
        case IndexMode.UPDATE => commitUpdateToSolr
        case IndexMode.DELETE => deleteFromSolr
        case _ => logger.info("Invalid solr index mode")
      }
      Committed
    } else {
      Continue
    }
  }



  def processSolrDoc(docList:Iterable[(String, String)]): SolrClientResult = {
    docList.foreach{
      docTuple => {
        Try[NodeSeq] {
          XML.loadString(docTuple._2)
        } match {
          case Success(xml) =>

            val fieldList: NodeSeq = xml \\ "field"

            val solrDoc = new SolrInputDocument()

            fieldList.foreach { solrField =>
              val fieldName = solrField.attribute("name").get.text
              val fieldValue = solrField.text
              solrDoc.addField(fieldName, fieldValue)
            }
            bufferUpdate.append(solrDoc)
          case Failure(error) =>
            //write to process topic
            logger.error(error.getLocalizedMessage)
            logger.error(docTuple._2)
        }
      }
    }

    if (bufferUpdate.length >= commitBufferLength) {
      commitUpdateToSolr
    } else {
      Continue
    }

  }

  private def commitUpdateToSolr: SolrClientResult = {
    Try[SolrClientResult] {
      client.add(collection, bufferUpdate.asJava)
      logger.info("send " + bufferUpdate.length.toString + " documents to solr")
      client.commit(collection)
      bufferUpdate = new ArrayBuffer[SolrInputDocument]()
      timeLastCommit = DateTime.now()
      Committed
    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(exception.getMessage)
        //write buffered messages to report topic
        Continue
    }
  }

  def processSolrDeletedDoc(docList:Iterable[(String, String)]): SolrClientResult = {
    docList.foreach{
      tuple =>  {
        //example id (ZBS)oai:alma.41SLSP_ZBS:9970007330105528
        val id = getIdFromKafkaKey(tuple._1)
        bufferDelete.addOne(id)
      }

    }
    if (bufferDelete.length >= commitBufferLength) {
      deleteFromSolr
    } else {
      Continue
    }


  }


  private def deleteFromSolr: SolrClientResult = {
    Try[SolrClientResult] {
      client.deleteById(collection, bufferDelete.asJava)
      logger.info("send " + bufferDelete.length.toString + " deletes to solr")
      client.commit(collection)
      bufferDelete = new ArrayBuffer[String]()
      timeLastCommit = DateTime.now()
      Committed

    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(exception.getMessage)
        Continue
    }
  }
}


object SolrJClientWrapper {
  def apply(mode: String, zkHosts: String, collection: String,
            connectionTimeout: String, socketTimeOut: String,
            zkChRoot: Option[String], nodeURL: String,
            solrCommitBufferlength: String,
            timeDurationForNextSolrCommit: String,
            indexmode: Utils.IndexMode.Value): SolrJClientWrapper = {

    if (mode == "cloud") {

      val client: SolrClient = new CloudSolrClient.Builder(zkHosts.split(",").
        toList.asJava,zkChRoot.toJava)
        .withConnectionTimeout(connectionTimeout.toInt)
        .withSocketTimeout(socketTimeOut.toInt)
        .build()

      new SolrJClientWrapper(client, collection,
        solrCommitBufferlength.toInt, timeDurationForNextSolrCommit.toInt, indexmode)

    } else {

      val client = new HttpSolrClient.Builder(nodeURL)
        .withConnectionTimeout(connectionTimeout.toInt)
        .withSocketTimeout(socketTimeOut.toInt)
        .build()
      new SolrJClientWrapper(client,collection,
        solrCommitBufferlength.toInt, timeDurationForNextSolrCommit.toInt, indexmode)
    }
  }

  def getIdFromKafkaKey(kafkaKey: String): String = kafkaKey.replace("(EXLNZ-41SLSP_NETWORK)", "")


}

sealed trait SolrClientResult extends Product
case object Committed extends SolrClientResult
case object Continue extends SolrClientResult


