/*
 * SOLR Indexer
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections

object Utils {

  object IndexMode extends Enumeration {
    protected case class Val(indexmode: String) extends super.Val
    import scala.language.implicitConversions
    implicit def valueToModeVal(x: Value): Val = x.asInstanceOf[Val]

    val UPDATE: Val = Val("UPDATE")
    val DELETE: Val = Val("DELETE")
  }

}
