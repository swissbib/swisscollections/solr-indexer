/*
 * SOLR Indexer
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import ch.swisscollections.SolrJClientWrapper
import org.scalatest.funsuite.AnyFunSuite


class SolrIndexerTests extends AnyFunSuite {


  test("Test Enumeration") {

    import ch.swisscollections.Utils._

    assert(IndexMode.withName("UPDATE") == IndexMode.UPDATE)
    assert(IndexMode.withName("DELETE") == IndexMode.DELETE)
    assertThrows[NoSuchElementException](IndexMode.withName("UPDATe"))
    assertThrows[NoSuchElementException](IndexMode.withName("DELETEe"))
  }

  test ("get IF from kafkakey") {
    val testKey = "(EXLNZ-41SLSP_NETWORK)9970007330105528"
    assert(SolrJClientWrapper.getIdFromKafkaKey(testKey) == "9970007330105528")
  }

}
