echo "Waiting for Kafka to come online..."

cub kafka-ready -b kafka:9092 1 20

# delete topics
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-enriched \
  --delete

# create topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-enriched \
  --replication-factor 1 \
  --partitions 4 \
  --create


sleep infinity
