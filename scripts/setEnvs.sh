#!/usr/bin/env bash

#simple utility to set environment variables for local development
export KAFKA_BOOTSTRAP_SERVERS=localhost:29092
export CLIENT_ID="app.solr-index"
export TOPIC_IN=swisscollections-enriched
export MODE='node'
export SOLR_COLLECTION='swisscollectionstest'
export CONNECTION_TIMEOUT='200000'
export SOCKET_TIMEOUT='600000'
#export NODE_URL='http://localhost:8984/solr'
export NODE_URL='https://solrtest.swisscollections.unibas.ch/solr'
export ZOOKEEPER_NAMESPACE='NONE'
export SOLR_COMMIT_BUFFER_LENGTH='500'
export TIME_DURATION_FOR_NEXT_SOLR_COMMIT='1'
export INDEXMODE='update'
export ZKHOSTS='not-used'

