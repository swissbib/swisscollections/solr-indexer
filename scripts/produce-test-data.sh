# load some data
docker-compose exec kafka bash -c "
  kafka-console-producer \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-enriched \
  --property 'parse.key=true' \
  --property 'key.separator=|' < records.txt"
